package springData.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import springData.bean.BeanA;
import springData.bean.BeanB;
import springData.bean.BeanC;
import springData.bean.BeanD;
import springData.bean.BeanE;
import springData.bean.BeanF;

@Configuration
@Import(SecondConfiguration.class)
public class FirstConfiguration {

  @Bean("firstBeanA")
  @DependsOn(value = {"getBeanB", "getBeanC"})
  public BeanA getBeanAFromBC(BeanB beanB, BeanC beanC) {
    BeanA beanA = new BeanA();
    beanA.setName(beanB.getName());
    beanA.setValue(beanC.getValue());
    return beanA;
  }

  @Bean
  @Qualifier("aBean")
  @DependsOn(value = {"getBeanB", "getBeanD"})
  public BeanA getBeanAFromBD(BeanB beanB, BeanD beanD) {
    BeanA beanA = new BeanA();
    beanA.setName(beanD.getName());
    beanA.setValue(beanB.getValue());
    return beanA;
  }

  @Bean
  @Primary
  @DependsOn(value = {"getBeanC", "getBeanD"})
  public BeanA getBeanAFromCD(BeanB beanC, BeanD beanD) {
    BeanA beanA = new BeanA();
    beanA.setName(beanC.getName());
    beanA.setValue(beanD.getValue());
    return beanA;
  }

  @Bean
  public BeanE getBeanEFirstMethod(BeanA aBean) {
    BeanE beanE = new BeanE();
    beanE.setName(aBean.getName());
    beanE.setValue(aBean.getValue());
    return beanE;
  }

  @Bean
  public BeanE getBeanESecondMethod(@Qualifier("getBeanAFromBD") BeanA aBean) {
    BeanE beanE = new BeanE();
    beanE.setValue(aBean.getValue());
    beanE.setName(aBean.getName());
    return beanE;
  }

  @Bean
  public BeanE getBeanEThirdMethod(@Qualifier("aBean") BeanA beanA) {
    BeanE beanE = new BeanE();
    beanE.setName(beanA.getName());
    beanE.setValue(beanA.getValue());
    return beanE;
  }

  @Bean
  @Lazy
  public BeanF getBeanF(){
    return new BeanF();
  }

}
