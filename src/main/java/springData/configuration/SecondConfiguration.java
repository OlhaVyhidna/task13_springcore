package springData.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;
import springData.bean.BeanB;
import springData.bean.BeanC;
import springData.bean.BeanD;

@Configuration
@PropertySource("beansValue.properties")
public class SecondConfiguration {
  @Value("${beanB.name}")
 private String beanBName;
  @Value("${beanB.value}")
  private int value;
  @Value("${beanC.name}")
 private String beanCName;
  @Value("${beanC.value}")
  private int beanCValue;
  @Value("${beanD.name}")
 private String beanDName;
  @Value("${beanD.value}")
  private int beanDValue;

  @Bean(initMethod = "init", destroyMethod = "destroy")
  @DependsOn("getBeanD")
  public BeanB getBeanB(){
    BeanB beanB = new BeanB();
    beanB.setName(beanBName);
    beanB.setValue(value);
    return beanB;
  }

  @Bean(initMethod = "init", destroyMethod = "destroy")
  @DependsOn("getBeanB")
  public BeanC getBeanC(){
    BeanC beanC = new BeanC();
    beanC.setName(beanCName);
    beanC.setValue(beanCValue);
    return beanC;
  }

  @Bean(initMethod = "init", destroyMethod = "destroy")
  public BeanD getBeanD(){
    BeanD beanD = new BeanD();
    beanD.setName(beanDName);
    beanD.setValue(beanDValue);
    return beanD;
  }

}
