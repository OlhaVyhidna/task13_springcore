package springData.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

  public void postProcessBeanFactory(
      ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
    BeanDefinition beanDefinition = configurableListableBeanFactory.getBeanDefinition("getBeanB");
    beanDefinition.setInitMethodName("newInit");
    getBeansConfiguration(configurableListableBeanFactory);
  }

  public void getBeansConfiguration(ConfigurableListableBeanFactory configurableListableBeanFactory){
    String[] beanDefinitionNames = configurableListableBeanFactory.getBeanDefinitionNames();
    for (String beanDefinitionName : beanDefinitionNames) {
      System.out.println("bean configuration  " + configurableListableBeanFactory.getBeanDefinition(beanDefinitionName));
    }
  }



}
