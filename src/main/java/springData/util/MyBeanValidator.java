package springData.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.validation.Validator;

public class MyBeanValidator implements BeanPostProcessor {

//  public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory)
//      throws BeansException {
//    String[] beanDefinitionNames = beanFactory.getBeanDefinitionNames();
//    for (String beanDefinitionName : beanDefinitionNames) {
//      System.out.println("Bean Factory " + beanDefinitionName);
//    }
//  }


  public Object postProcessBeforeInitialization(Object bean, String beanName)
      throws BeansException {
    return bean;
  }

  public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
    if (bean instanceof BeanValidator){
      ((BeanValidator)bean).validate();
    }
    return bean;
  }
}
