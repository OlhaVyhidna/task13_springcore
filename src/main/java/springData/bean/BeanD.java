package springData.bean;

import springData.util.BeanValidator;

public class BeanD implements BeanValidator {

  private String name;
  private int value;

  public void init(){
    System.out.println("BeanD init method");
  }

  public void destroy(){
    System.out.println("BeanD destroy method");
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanD{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public void validate() {
    if (this.name==null){
      System.out.println("Field name should be not null");
    }
    if (this.value<=0){
      System.out.println("Field value should be positive");
    }
  }
}
