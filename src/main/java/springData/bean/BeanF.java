package springData.bean;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import springData.util.BeanValidator;

public class BeanF implements Serializable, BeanValidator {

  private String name;
  private int value;

  @PostConstruct
  public void postConstruct(){
    System.out.println("BeanF postConstruct method");
  }

  @PreDestroy
  public void preDestroy(){
    System.out.println("BeanF preDestroy method");
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanF{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public void validate() {
    if (this.name==null){
      System.out.println("Field name should be not null");
    }
    if (this.value<=0){
      System.out.println("Field value should be positive");
    }
  }
}
