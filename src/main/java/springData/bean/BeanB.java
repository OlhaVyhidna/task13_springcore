package springData.bean;

import springData.util.BeanValidator;

public class BeanB implements BeanValidator {

  private String name;
  private int value;

  public void init(){
    System.out.println("BeanB init method");
  }

  public void newInit(){
    System.out.println("New init method for BeanB");
  }

  public void destroy(){
    System.out.println("BeanB destroy method");
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanB{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public void validate() {
    if (this.name==null){
      System.out.println("Field name should be not null");
    }
    if (this.value<=0){
      System.out.println("Field value should be positive");
    }
  }
}
