package springData.bean;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import springData.util.BeanValidator;

public class BeanA implements BeanValidator, InitializingBean, DisposableBean {

  private String name;
  private int value;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return "BeanA{" +
        "name='" + name + '\'' +
        ", value=" + value +
        '}';
  }

  public void destroy() throws Exception {
    System.out.println("BeanA destroy method from disposable interface");
  }


  public void afterPropertiesSet() throws Exception {
    System.out.println("BeanA afterPropertiesSet method");
  }

  public void validate() {
    if (this.name==null){
      System.out.println("Field name should be not null");
    }
    if (this.value<=0){
      System.out.println("Field value should be positive");
    }
  }
}
