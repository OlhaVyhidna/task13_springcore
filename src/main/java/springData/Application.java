package springData;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import springData.configuration.FirstConfiguration;
import springData.util.MyBeanFactoryPostProcessor;
import springData.util.MyBeanValidator;

public class Application {



  public static void main(String[] args) {
    ApplicationContext context = new AnnotationConfigApplicationContext(FirstConfiguration.class,
        MyBeanFactoryPostProcessor.class, MyBeanValidator.class);
    String[] beanDefinitionNames = context.getBeanDefinitionNames();
    for (String beanDefinitionName : beanDefinitionNames) {
      System.out.println(context.getBean(beanDefinitionName));
    }
  }



}
